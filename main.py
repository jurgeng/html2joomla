#!/usr/bin/env python3

import secrets
import requests
import json
import logging
import argparse
from random import randint
import os
from lxml.html.clean import Cleaner
import string
import re

parser = argparse.ArgumentParser(description="Moving an HTML tree structure into Joomla content")

parser.add_argument("-s", dest="src_path", default=r"C:\Users\gangs\DFD\freedom days\wiki",
                    help="Location of the path containing the page folders")
parser.add_argument("-t", dest="target_domain", default=r"http://joomla.local",
                    help="Path storing the output results")
parser.add_argument("-c", dest="catid", default=2,
                    help="ID of root category containing all child categories and articles")
parser.add_argument('-n', dest="event_name", default="Software Freedom Day",
                    help="Full name of the event being parsed, defaults to 'Software Freedom Day'")
parser.add_argument('-m', dest="event_short", default="sfd",
                    help="Shorthand (mini) of the event name. Defaults to: 'sfd'")
parser.add_argument("-o", dest="online_path", default="/images",
                    help="Base path where images will be found online")
parser.add_argument('-u', dest="user_id", default=42,
                    help="User ID of account to which the article should be assigned")
parser.add_argument("-d", dest="detail", default="debug",
                    choices=['warning', 'error', 'info', 'info'],
                    help="Options: warning, error, info, debug")
parser.add_argument("-b", dest="base_category", default=1,
                    help="The ID of the base category in which all other events will be stored (1 = root)")

args = parser.parse_args()

# Define the mapping between 'detail' argument values and logging levels
detail_to_logging_level = {
    'warning': logging.WARNING,
    'error': logging.ERROR,
    'info': logging.INFO,
    'debug': logging.DEBUG
}

# Configure logging based on the 'detail' argument
logging_level = detail_to_logging_level.get(args.detail, logging.DEBUG)
logging.basicConfig(level=logging_level, format='%(asctime)s - %(levelname)s - %(message)s')

directory = os.fsencode(args.src_path)
catid = args.catid


def convert_to_object(metadataPath="metadata.json", htmlPath="index.html"):

    try:
        # Open and read the file
        with open(metadataPath, 'r') as metadataFile:
            content = json.load(metadataFile)
        with open(htmlPath, 'r') as htmlFile:
            html = htmlFile.read()

        cleaner = Cleaner()

        content.update({
        'articletext': cleaner.clean_html(html),   # Will be used by Article
        'description' : cleaner.clean_html(html),  # Will be used by Category
        'parent_id' : catid,  # will be used by Category
        'catid' : catid,      # will be used by Article
        'created_by' : args.user_id,
        'modified_by': args.user_id,
        'alias': create_alias(content['title']),
        'language': "*", # To be refined later
        'metadesc': "",
        'metadata': {
            "robots": "index, follow",
            "author": content['created_by_alias'],
            "rights": "CC-BY-SA 4.0"
        },
        'state': 1,
        'published': 1,
        'featured': 0,
        })

        if "title" not in content:
            text = content['articletext']
            pattern = r"<h1.*?>(.*?)<\/h1>"
            match = re.search(pattern, text, re.IGNORECASE)
            if match:
                logging.info("Found:", match.group(1))
                content['title'] = match.group(1)
            else:
                logging.warning("No title found for article - generating random number")
                content['title'] = "TITLE " & str(randint(10000000, 99999999))

        # TODO: Remove this code when the block is working right.
        # Temporary block to randomize field during development. Should be removed when actually using it
        randomizer = str(randint(1000, 9999))
        for key, value in content.items():
            if isinstance(value, str):
                content[key] = value.replace("RND", randomizer)
        # END OF TEMPORARY CODE

        logging.info(content)

    except FileNotFoundError:
        content = False
        logging.error(f"The file {metadataPath} was not found.")

    except json.JSONDecodeError:
        content = False
        logging.error(f"The file {metadataPath} does not contain valid JSON.")

    except Exception as e:
        content = False
        logging.error(f"An error occurred: {e}")

    return content


post_create_article = "/api/index.php/v1/content/articles"
post_create_category = "/api/index.php/v1/content/categories"

def create_alias(text):
    return text.lower().translate(str.maketrans('', '', string.punctuation)).replace(' ', '-')

def create_joomla_article(metadata):
    # Headers
    headers = {
        'Content-Type': 'application/json',
        'Authorization': f'Bearer {secrets.key}'
    }

    try:
        # Make a POST request
        response = requests.post(secrets.url + post_create_article, headers=headers, data=json.dumps(metadata))

        # Check if the request was successful
        if response.status_code == 200:
            print("Request successful. Response:", response.json())
        else:
            print(f"Request failed: {response.status_code} - {response.reason}")
    except requests.exceptions.RequestException as e:
        print(f"Error making request: {e}")


def create_joomla_category(metadata):
    # Additional parameter "parent" -> by default a new category is a root category.
    # Otherwise, the parent must be passed on.

    global catid
    # TODO: pick the right parent category and propagate the new catid

    # Headers
    headers = {
        'Content-Type': 'application/json',
        'Authorization': f'Bearer {secrets.key}'
    }

    try:
        # Make a POST request
        response = requests.post(secrets.url + post_create_category, headers=headers, data=json.dumps(metadata))

        # Check if the request was successful
        if response.status_code == 200:
            logging.debug("Request successful. Response:", response.json())
        else:
            logging.error(f"Request failed. Status code: {response.status_code}")
    except requests.exceptions.RequestException as e:
        logging.error(f"Error making request: {e}")


def process_directory(base_path):
    # Set base values
    year = ""
    # Check if the given path is a directory
    if not os.path.isdir(base_path):
        logging.error(f"{base_path} is not a directory.")
        return

    # Check for subdirectories
    has_subdirs = any(os.path.isdir(os.path.join(base_path, name)) for name in os.listdir(base_path))

    index_file = os.path.join(base_path, "index.html")
    metadata_file = os.path.join(base_path, "metadata.json")

    content = convert_to_object(metadataPath=metadata_file, htmlPath=index_file)

    if has_subdirs:
        # Create a category for the current directory
        if content:
            create_joomla_category(metadata=content)
        else:
            logging.warning(f"Skipped creation of category {base_path}")

        # Recursively process each subdirectory
        for name in os.listdir(base_path):
            path = os.path.join(base_path, name)
            if os.path.isdir(path):
                process_directory(path)
    else:
        # If no subdirectories, create an article for the current directory
        create_joomla_article(metadata=content)


def main():
    # Step 1: walk through directory tree recursively
    #         folders with subfolders become a category
    #         folders without subfolders (ignoring images folder) become article
    process_directory(args.src_path)

    # Step 2: parse metadata file and index file and turn it into a json object for the API call
    content = convert_to_object(metadataPath="metadata.json")

    ## Step 3: depending on context - create category or article
    create_joomla_article(metadata=content)


# Add the main code of your script here
if __name__ == "__main__":
    main()
