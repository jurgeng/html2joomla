#!/usr/bin/env python3

import secrets
import requests
import json
import logging
import argparse
from random import randint


parser = argparse.ArgumentParser(description="Parse a moin page dump")

parser.add_argument("-s", dest="src_path", default=r"C:\Users\gangs\DFD\freedom days\dfd2017\dfd2017\core\data\pages",
                    help="Location of the path containing the page folders")
parser.add_argument("-t", dest="target_path", default=r"C:\Users\gangs\DFD\freedom days\wiki",
                    help="Path storing the output results")
parser.add_argument("-o", dest="online_path", default="/images",
                    help="Base path where images will be found online")
parser.add_argument("-d", dest="detail", default="debug",
                    choices=['warning', 'error', 'info', 'info'],
                    help="Options: warning, error, info, debug")

args = parser.parse_args()

# Define the mapping between 'detail' argument values and logging levels
detail_to_logging_level = {
    'warning': logging.WARNING,
    'error': logging.ERROR,
    'info': logging.INFO,
    'debug': logging.DEBUG
}

# Configure logging based on the 'detail' argument
logging_level = detail_to_logging_level.get(args.detail, logging.DEBUG)
logging.basicConfig(level=logging_level, format='%(asctime)s - %(levelname)s - %(message)s')


commandGetArticle = "/api/index.php/v1/article"
commandGetArticles = "/api/index.php/v1/content/articles"
commandPostArticle = "/api/index.php/v1/content/articles"


def main():
    # Headers
    headers = {
        'Authorization': f'Bearer {secrets.key}'
    }

    try:
        # Get List of articles
        response = requests.get(secrets.url + "/api/index.php/v1/content/articles/37", headers=headers)
        print(response.json())
        # Check if the request was successful
        if response.status_code == 200:
            print("Request successful. Response:", response.json())
        else:
            print(f"Request failed. Status code: {response.status_code}")
    except requests.exceptions.RequestException as e:
        print(f"Error making request: {e}")


# Add the main code of your script here
if __name__ == "__main__":
    main()
